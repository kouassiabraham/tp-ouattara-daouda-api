<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AbonneController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json(Abonne::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "nom" => "required|max:15",
            "prenom" => "required|min:5",
            "email" => "required|email",
            "contact" => "required",
        ]);

        if ($validate->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validate->errors()->first()
            ]);
        }

        $abonne = Abonne::create([
            "nom" => $request->get('nom'),
            "prenom" => $request->get('prenom'),
            "email" => $request->get('email'),
            "contact" => $request->get('contact'),
        ]);

        return response()->json([
            'error' => false,
            'message' => "Abonné ajouté",
            'data' => $abonne
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Abonne $abonne)
    {
        return $abonne;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Abonne $abonne)
    {

        $validate = Validator::make($request->all(), [
            "nom" => "required|max:15",
            "prenom" => "required|min:5",
            "email" => "required|email",
            "contact" => "required",
        ]);

        if ($validate->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validate->errors()->first()
            ]);
        }

        $abonne->update([
            "nom" => $request->get('nom'),
            "prenom" => $request->get('prenom'),
            "email" => $request->get('email'),
            "contact" => $request->get('contact'),
        ]);

        return response()->json([
            'error' => false,
            'message' => "Abonné modifié",
            'data' => $abonne
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Abonne $abonne)
    {

        $abonne->delete();

        return response()->json([
            'error' => false,
            'message' => "Abonné supprimé",
        ]);
    }
}
