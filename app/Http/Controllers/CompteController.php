<?php

namespace App\Http\Controllers;

use App\Models\Compte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompteController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->has("iban")) {
            $query = Compte::whereRaw("CONCAT(banque,agence,numero,rib) = ?", $request->input("iban"))->get();
            return $query;
        }
        return response()->json(Compte::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "abonne_id" => "required",
            "libelle" => "required|max:10",
            "description" => "required",
            "agence" => "required",
            "banque" => "required",
            "numero" => "required",
            "rib" => "required",
            "montant" => "required",
            "domiciliation" => "required",
        ]);

        if ($validate->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validate->errors()->first()
            ]);
        }

        $compte = Compte::create([
            "abonne_id" => $request->get('abonne_id'),
            "libelle" => $request->get('libelle'),
            "description" => $request->get('description'),
            "agence" => $request->get('agence'),
            "banque" => $request->get('banque'),
            "numero" => $request->get('numero'),
            "rib" => $request->get('rib'),
            "montant" => $request->get('montant'),
            "domiciliation" => $request->get('domiciliation'),
        ]);

        return response()->json([
            'error' => false,
            'message' => "Compte ajouté",
            'data' => $compte
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Compte $compte)
    {
        return $compte;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Compte $compte)
    {
        $validate = Validator::make($request->all(), [
            "abonne_id" => "required",
            "libelle" => "required|max:10",
            "description" => "required",
            "agence" => "required",
            "banque" => "required",
            "numero" => "required",
            "rib" => "required",
            "montant" => "required",
            "domiciliation" => "required",
        ]);

        if ($validate->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validate->errors()->first()
            ]);
        }

        $compte->update([
            "abonne_id" => $request->get('abonne_id'),
            "libelle" => $request->get('libelle'),
            "description" => $request->get('description'),
            "agence" => $request->get('agence'),
            "banque" => $request->get('banque'),
            "numero" => $request->get('numero'),
            "rib" => $request->get('rib'),
            "montant" => $request->get('montant'),
            "domiciliation" => $request->get('domiciliation'),
        ]);

        return response()->json([
            'error' => false,
            'message' => "Compte modifié",
            'data' => $compte
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Compte $compte)
    {
        $compte->delete();

        return response()->json([
            'error' => false,
            'message' => "Compte supprimé",
        ]);
    }
}
