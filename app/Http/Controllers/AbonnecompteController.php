<?php

namespace App\Http\Controllers;

use App\Models\Abonne;
use App\Models\Compte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AbonnecompteController extends Controller
{
    public function index()
    {
        $abonnes = Abonne::all();
        foreach ($abonnes as $abonne) {
            $abonne = $abonne->comptes;
        }
        return $abonnes;
    }

    public function show(String $id)
    {
        $abonne = Abonne::find($id);

        if ($abonne == null) {
            return response()->json([
                "error" => true,
                "message" => "Aucune donnée trouvé"
            ], 404);
        }

        $abonne["comptes"] = $abonne->comptes;
        return $abonne;
    }

    public function liaisons(Request $request)
    {
        $validate = Validator::make($request->all(), [
            "abonneId" => "required",
            "compteId" => "required",
        ]);

        if ($validate->fails()) {
            return response()->json([
                "error" => true,
                "message" => $validate->errors()->first()
            ]);
        }

        $compte = Compte::find($request->get('compteId'));

        if ($compte == null) {
            return response()->json([
                "error" => true,
                "message" => "Aucune donnée trouvé"
            ], 404);
        }

        $compte->update([
            "abonne_id" => $request->get('abonneId'),
        ]);

        return response()->json([
            'error' => false,
            'message' => "Liaison éffectuée",
        ]);
    }

    public function stats()
    {
        $comptes = Compte::all();
        return response()->json([
            "sumCompte" => $comptes->sum("montant"),
            "nbrCompte" => $comptes->count(),
            "min" => $comptes->min("montant"),
            "max" => $comptes->max("montant"),
            "moy" => $comptes->avg("montant")
        ]);
    }

    public function statsAbonne(string $id)
    {
        $abonne = Abonne::find($id);
        $abonne["stats"] = [
            "sumCompte" => $abonne->comptes->sum("montant"),
            "nbrCompte" => $abonne->comptes->count(),
            "min" => $abonne->comptes->min("montant"),
            "max" => $abonne->comptes->max("montant"),
            "moy" => $abonne->comptes->avg("montant")
        ];
        unset($abonne["comptes"]);
        return $abonne;
    }
}
