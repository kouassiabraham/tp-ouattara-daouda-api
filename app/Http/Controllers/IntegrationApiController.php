<?php

namespace App\Http\Controllers;

use App\Models\Personne;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class IntegrationApiController extends Controller
{
    public function index()
    {
        $personne = Personne::all()->random();
        $response = Http::get("https://rawcdn.githack.com/kamikazechaser/administrative-divisions-db/master/api/" . $personne->indicatif . ".json");
        $personne["region"] = json_decode($response);
        return $personne;
    }
}
