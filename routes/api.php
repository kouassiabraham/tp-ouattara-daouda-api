<?php

use App\Http\Controllers\AbonnecompteController;
use App\Http\Controllers\AbonneController;
use App\Http\Controllers\CompteController;
use App\Http\Controllers\IntegrationApiController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/sanctum/token', function (Request $request) {
    $validate = Validator::make($request->all(), [
        'email' => 'required|email',
        'password' => 'required',
    ]);

    if ($validate->fails()) {
        return response()->json([
            "error" => true,
            "message" => $validate->errors()->first()]);
    }

    $user = User::where('email', $request->email)->first();

    if (!$user || !Hash::check($request->password, $user->password)) {
        return response()->json([
            "error" => true,
            "message" => "Email ou mot de passe incorrect"]);
    }

    return response()->json([
        "error" => false,
        "token" => $user->createToken($request->email)->plainTextToken]);
});


Route::middleware('auth:sanctum')->prefix("v1")->group(function () {
    Route::get('abonnes/comptes', [AbonnecompteController::class, 'index']);
    Route::apiResources(["abonnes" => AbonneController::class, "comptes" => CompteController::class]);
    Route::get('abonnes/{id}/comptes', [AbonnecompteController::class, 'show']);
    Route::post('liaisons', [AbonnecompteController::class, 'liaisons']);
    Route::get('stats', [AbonnecompteController::class, 'stats']);
    Route::get('stats/abonnes/{id}', [AbonnecompteController::class, 'statsAbonne']);
    Route::get('personnes/random', [IntegrationApiController::class, 'index']);
});