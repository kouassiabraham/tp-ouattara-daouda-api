<?php

namespace Database\Factories;

use App\Models\Abonne;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Compte>
 */
class CompteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $abonne = Abonne::all()->random();
        return [
            "abonne_id" => $abonne->id,
            "libelle" => fake()->word(),
            "description" => fake()->sentence(),
            "agence" => fake()->randomNumber(4),
            "banque" => "CI".fake()->randomNumber(3),
            "numero" => "0".fake()->randomNumber(9)."0",
            "rib" => fake()->randomNumber(2),
            "montant" => fake()->randomNumber(6),
            "domiciliation" => fake()->address(),
        ];
    }
}
